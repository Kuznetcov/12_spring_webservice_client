package com.client.xml;


import java.util.List;

import com.webservice.Client;
import com.webservice.Deal;
import com.webservice.SoapTest;

public class SoapTestClientImpl {

  private SoapTest service;

  public void setService(SoapTest service) {
      this.service = service;
  }

  public List<Client> foo() {
    return (List<Client>) service.getClient(1);
  }
  
  public List<Deal> deals(Integer id){
    return (List<Deal>) service.getDealsByClientID(id);
  }
  
}
