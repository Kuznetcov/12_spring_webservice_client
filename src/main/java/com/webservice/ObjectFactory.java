
package com.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SayHello_QNAME = new QName("http://webservice.com/", "sayHello");
    private final static QName _GetDealsByClientIDResponse_QNAME = new QName("http://webservice.com/", "getDealsByClientIDResponse");
    private final static QName _GetClient_QNAME = new QName("http://webservice.com/", "getClient");
    private final static QName _SayHelloResponse_QNAME = new QName("http://webservice.com/", "sayHelloResponse");
    private final static QName _AddDeal_QNAME = new QName("http://webservice.com/", "addDeal");
    private final static QName _AddPlaceEventResponse_QNAME = new QName("http://webservice.com/", "addPlaceEventResponse");
    private final static QName _GetClientResponse_QNAME = new QName("http://webservice.com/", "getClientResponse");
    private final static QName _GetDealsByClientID_QNAME = new QName("http://webservice.com/", "getDealsByClientID");
    private final static QName _AddDealResponse_QNAME = new QName("http://webservice.com/", "addDealResponse");
    private final static QName _AddPlaceEvent_QNAME = new QName("http://webservice.com/", "addPlaceEvent");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SayHello }
     * 
     */
    public SayHello createSayHello() {
        return new SayHello();
    }

    /**
     * Create an instance of {@link GetDealsByClientIDResponse }
     * 
     */
    public GetDealsByClientIDResponse createGetDealsByClientIDResponse() {
        return new GetDealsByClientIDResponse();
    }

    /**
     * Create an instance of {@link GetClient }
     * 
     */
    public GetClient createGetClient() {
        return new GetClient();
    }

    /**
     * Create an instance of {@link SayHelloResponse }
     * 
     */
    public SayHelloResponse createSayHelloResponse() {
        return new SayHelloResponse();
    }

    /**
     * Create an instance of {@link GetDealsByClientID }
     * 
     */
    public GetDealsByClientID createGetDealsByClientID() {
        return new GetDealsByClientID();
    }

    /**
     * Create an instance of {@link AddDealResponse }
     * 
     */
    public AddDealResponse createAddDealResponse() {
        return new AddDealResponse();
    }

    /**
     * Create an instance of {@link AddPlaceEvent }
     * 
     */
    public AddPlaceEvent createAddPlaceEvent() {
        return new AddPlaceEvent();
    }

    /**
     * Create an instance of {@link AddDeal }
     * 
     */
    public AddDeal createAddDeal() {
        return new AddDeal();
    }

    /**
     * Create an instance of {@link AddPlaceEventResponse }
     * 
     */
    public AddPlaceEventResponse createAddPlaceEventResponse() {
        return new AddPlaceEventResponse();
    }

    /**
     * Create an instance of {@link GetClientResponse }
     * 
     */
    public GetClientResponse createGetClientResponse() {
        return new GetClientResponse();
    }

    /**
     * Create an instance of {@link Deal }
     * 
     */
    public Deal createDeal() {
        return new Deal();
    }

    /**
     * Create an instance of {@link Event }
     * 
     */
    public Event createEvent() {
        return new Event();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHello }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "sayHello")
    public JAXBElement<SayHello> createSayHello(SayHello value) {
        return new JAXBElement<SayHello>(_SayHello_QNAME, SayHello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDealsByClientIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "getDealsByClientIDResponse")
    public JAXBElement<GetDealsByClientIDResponse> createGetDealsByClientIDResponse(GetDealsByClientIDResponse value) {
        return new JAXBElement<GetDealsByClientIDResponse>(_GetDealsByClientIDResponse_QNAME, GetDealsByClientIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "getClient")
    public JAXBElement<GetClient> createGetClient(GetClient value) {
        return new JAXBElement<GetClient>(_GetClient_QNAME, GetClient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHelloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "sayHelloResponse")
    public JAXBElement<SayHelloResponse> createSayHelloResponse(SayHelloResponse value) {
        return new JAXBElement<SayHelloResponse>(_SayHelloResponse_QNAME, SayHelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDeal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "addDeal")
    public JAXBElement<AddDeal> createAddDeal(AddDeal value) {
        return new JAXBElement<AddDeal>(_AddDeal_QNAME, AddDeal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPlaceEventResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "addPlaceEventResponse")
    public JAXBElement<AddPlaceEventResponse> createAddPlaceEventResponse(AddPlaceEventResponse value) {
        return new JAXBElement<AddPlaceEventResponse>(_AddPlaceEventResponse_QNAME, AddPlaceEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetClientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "getClientResponse")
    public JAXBElement<GetClientResponse> createGetClientResponse(GetClientResponse value) {
        return new JAXBElement<GetClientResponse>(_GetClientResponse_QNAME, GetClientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDealsByClientID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "getDealsByClientID")
    public JAXBElement<GetDealsByClientID> createGetDealsByClientID(GetDealsByClientID value) {
        return new JAXBElement<GetDealsByClientID>(_GetDealsByClientID_QNAME, GetDealsByClientID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddDealResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "addDealResponse")
    public JAXBElement<AddDealResponse> createAddDealResponse(AddDealResponse value) {
        return new JAXBElement<AddDealResponse>(_AddDealResponse_QNAME, AddDealResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPlaceEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.com/", name = "addPlaceEvent")
    public JAXBElement<AddPlaceEvent> createAddPlaceEvent(AddPlaceEvent value) {
        return new JAXBElement<AddPlaceEvent>(_AddPlaceEvent_QNAME, AddPlaceEvent.class, null, value);
    }

}
