
package com.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addDeal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addDeal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dealID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="employeeID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="parkingPlaceID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="carNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addDeal", propOrder = {
    "dealID",
    "date",
    "clientID",
    "employeeID",
    "parkingPlaceID",
    "carNumber"
})
public class AddDeal {

    protected int dealID;
    @XmlElement(required = true)
    protected String date;
    protected int clientID;
    protected int employeeID;
    protected int parkingPlaceID;
    protected int carNumber;

    /**
     * Gets the value of the dealID property.
     * 
     */
    public int getDealID() {
        return dealID;
    }

    /**
     * Sets the value of the dealID property.
     * 
     */
    public void setDealID(int value) {
        this.dealID = value;
    }

    /**
     * Gets the value of the date property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Gets the value of the clientID property.
     * 
     */
    public int getClientID() {
        return clientID;
    }

    /**
     * Sets the value of the clientID property.
     * 
     */
    public void setClientID(int value) {
        this.clientID = value;
    }

    /**
     * Gets the value of the employeeID property.
     * 
     */
    public int getEmployeeID() {
        return employeeID;
    }

    /**
     * Sets the value of the employeeID property.
     * 
     */
    public void setEmployeeID(int value) {
        this.employeeID = value;
    }

    /**
     * Gets the value of the parkingPlaceID property.
     * 
     */
    public int getParkingPlaceID() {
        return parkingPlaceID;
    }

    /**
     * Sets the value of the parkingPlaceID property.
     * 
     */
    public void setParkingPlaceID(int value) {
        this.parkingPlaceID = value;
    }

    /**
     * Gets the value of the carNumber property.
     * 
     */
    public int getCarNumber() {
        return carNumber;
    }

    /**
     * Sets the value of the carNumber property.
     * 
     */
    public void setCarNumber(int value) {
        this.carNumber = value;
    }

}
